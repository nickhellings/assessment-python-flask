from flask_restx import Namespace

tasks_namespace = Namespace('Tasks', path="/tasks/")

all_namespaces = [tasks_namespace]
