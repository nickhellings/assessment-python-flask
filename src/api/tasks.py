from flask import abort
from requests import codes

from api.arguments.tasks import task_arguments, task_get_arguments
from api.models.tasks import task_model, task_create_model
from api.namespaces import tasks_namespace
from flask_restx import Resource

from domain.tasks import TasksService
from domain.models.tasks import Task as TaskModel
from exceptions import BadRequestError, NotFoundError


@tasks_namespace.route('/<int:id>', methods=["GET", "PUT", "DELETE"])
class Task(Resource):

    @tasks_namespace.response(codes.NOT_FOUND, "Task not found")
    @tasks_namespace.response(codes.OK, "Retrieved task", task_model)
    @tasks_namespace.marshal_with(task_model)
    def get(self, id):
        task = TasksService().get_task(id)
        if not task:
            abort(codes.NOT_FOUND, f"Task with id '{id}' could not be found.")

        return task, codes.OK

    @tasks_namespace.response(codes.NOT_FOUND, "Task not found")
    @tasks_namespace.response(codes.NO_CONTENT, "No Content")
    def delete(self, id):
        try:
            TasksService().delete_task(id)
        except NotFoundError:
            abort(codes.NOT_FOUND, f"Task with id '{id}' could not be found.")

        return "Deleted", codes.NO_CONTENT

    @tasks_namespace.response(codes.BAD_REQUEST, "Bad request")
    @tasks_namespace.response(codes.OK, "Task updated", task_model)
    @tasks_namespace.response(codes.NOT_FOUND, "Task not found")
    @tasks_namespace.marshal_with(task_model)
    @tasks_namespace.expect(task_model)
    def put(self, id):
        task = TaskModel.from_json(task_arguments.parse_args())
        try:
            updated_task = TasksService().update_task(id, task)
        except BadRequestError:
            abort(codes.BAD_REQUEST, f"Invalid parameters passed.")
        except NotFoundError:
            abort(codes.NOT_FOUND, f"Task with id '{id}' could not be found.")

        return updated_task, codes.OK


@tasks_namespace.route('', methods=["GET", "POST"])
class Tasks(Resource):

    @tasks_namespace.marshal_with(task_model, as_list=True)
    @tasks_namespace.response(200, "Retrieved tasks", task_model)
    @tasks_namespace.expect(task_get_arguments)
    def get(self):
        parsed_arguments = task_get_arguments.parse_args()
        page = parsed_arguments.get('page') 
        per_page = parsed_arguments.get('per_page')
        filter = parsed_arguments.get("filter")
        sort_by = parsed_arguments.get("sort_by")
        tasks = TasksService().get_filtered_tasks(page, per_page, filter, sort_by)

        return tasks, codes.OK

    @tasks_namespace.response(codes.BAD_REQUEST, "Bad request")
    @tasks_namespace.response(codes.CREATED, "Task created", task_model)
    @tasks_namespace.marshal_with(task_model)
    @tasks_namespace.expect(task_create_model)
    def post(self):
        parsed_arguments = task_arguments.parse_args()
        task = TaskModel.from_json(parsed_arguments)

        created_task = TasksService().create_task(task)

        return created_task, codes.CREATED
