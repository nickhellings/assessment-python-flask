from flask_restx import fields

from api.namespaces import tasks_namespace


task_model = tasks_namespace.model("Task", dict(
    id=fields.Integer(),
    name=fields.String(),
    description=fields.String(),
    status=fields.String(),
))


task_create_model = tasks_namespace.model('TaskCreate', dict(
    name=fields.String(),
    description=fields.String(),
))