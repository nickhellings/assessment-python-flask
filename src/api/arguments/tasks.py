from flask_restx import reqparse

from domain.tasks import DEFAULT_PER_PAGE, DEFAULT_SORT_BY

task_arguments = reqparse.RequestParser(bundle_errors=True)
task_arguments.add_argument('name', type=str, required=True, location=('form', 'json'))
task_arguments.add_argument('description', type=str, required=False, location=('form', 'json'))
task_arguments.add_argument('status', type=str, required=False, location=('form', 'json'))


task_get_arguments = reqparse.RequestParser(bundle_errors=True)
task_get_arguments.add_argument("page", type=int, required=False, location=('args', 'headers'))
task_get_arguments.add_argument("per_page", type=int, required=False, default=DEFAULT_PER_PAGE, location=('args', 'headers'))
task_get_arguments.add_argument("sort_by", type=str, required=False, default=DEFAULT_SORT_BY, location=('args', 'headers'))
# Proper filtering is a lot of work to implement. You'd normally want to be able to filter each field with multiple operators. This would need additional code to translate into actual filters in the database layer.
# For now just filter the name field to contain a certain string.
task_get_arguments.add_argument("filter", type=str, required=False, location=('args',))