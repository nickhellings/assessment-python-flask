from typing import Optional, List
from domain.models.tasks import Task
from exceptions import NotFoundError

from repository.models.tasks import TaskEntity
from db import db


class TasksRepository:
    def get_all(self) -> List[TaskEntity]:
        task_entities = TaskEntity.query.all()
        tasks = [TaskEntity.to_domain(task_entity) for task_entity in task_entities]
        return tasks
    
    def get_by_filter(self, page, per_page, filter, sort_by) -> List[TaskEntity]:
        query = db.select(TaskEntity)
        if sort_by is not None:
            query = query.order_by(getattr(TaskEntity, sort_by))
        
        if filter is not None:
            query = query.filter(TaskEntity.name.ilike(f"%{filter}%"))

        if page:
            task_entities = db.paginate(query, page=page, per_page=per_page).items
        else:
            task_entities = db.session.execute(query).scalars().all()
        
        tasks = [TaskEntity.to_domain(task_entity) for task_entity in task_entities]

        return tasks

    def get(self, id: str) -> Optional[TaskEntity]:
        task_entity = TaskEntity.query.get(id)
        if not task_entity:
            return None
        
        task = TaskEntity.to_domain(task_entity)
        return task
    
    def create(self, task: Task) -> TaskEntity:
        entity = TaskEntity.from_domain(task)
        # This is bad practice. Literally stated by sql alchemy to not do this: 
        # https://docs.sqlalchemy.org/en/20/orm/session_basics.html#when-do-i-construct-a-session-when-do-i-commit-it-and-when-do-i-close-it
        # If i have time left, move the session management to a higher level, each
        # request can be wrapped with this handling. Doing that would require to also catch errors and return them as specific error codes.
        try:
            db.session.add(entity)
            db.session.commit()
        except:
            db.session.rollback()
            raise

        task = TaskEntity.to_domain(entity)
        return task
    
    def update(self, id, task: Task):
        # I would actually like to figure out how we can better maintain separation between domain and repository layers. Currently when updating we need to retrieve the existing object because we are using an ORM. 
        # And then we need to update the retrieved entity with the new properties. In a case where you would like to do validation you would need to do this in the domain layer. 
        # However pulling the repository models into the domain layer is bad practice. In that case you would need to do the same functionality twice perhaps, retrieve the existing object from the db to perform validation. 
        # And after this send the new object to the repository layer, where we will need to retrieve the entity again and update it's fields before comitting it?
        entity = TaskEntity.query.get(id)
        if not entity:
            raise NotFoundError(f"Task with id '{id}' could not be found.")
            
        try:
            # Definitely don't want to do this. But seeing as I'm running out of time. This is linked with the comment above.
            entity.name = task.name
            entity.description = task.description
            entity.status = task.status
            db.session.merge(entity)
            db.session.commit()
        except:
            db.session.rollback()
            raise

        task = TaskEntity.to_domain(entity)
        return task

    def delete(self, id):
        entity = TaskEntity.query.get(id)
        if not entity:
            raise NotFoundError(f"Task with id '{id}' could not be found.")
            
        try:
            db.session.delete(entity)
            db.session.commit()
        except:
            db.session.rollback()
            raise
