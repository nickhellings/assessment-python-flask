from sqlalchemy import Column, String, Integer
from domain.models.tasks import Task

from db import db


class TaskEntity(db.Model):
    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String, nullable=False, index=True)
    description = Column(String, nullable=True, index=True)
    status = Column(String, nullable=True, index=True)
    
    @classmethod
    def from_domain(cls, task):
        entity = cls(id=task.id, name=task.name, description=task.description, status=task.status)
        return entity

    def to_domain(self):
        task = Task(id=self.id, name=self.name, description=self.description, status=self.status)
        return task
