# More elaborate exceptions could be added here, which allow for custom text to be added and which can be forwarded to the user in the frontend for display and debugging purposes.

class BadRequestError(Exception):
    pass

class NotFoundError(Exception):
    pass
