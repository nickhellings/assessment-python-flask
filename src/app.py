from flask import Flask, Blueprint
from flask_restx import Api

from db import db
from api.namespaces import all_namespaces


def init_app():
    app = Flask(__name__)
    init_api(app)
    init_db(app)

    return app


def init_api(app):
    # Authorization and authentication can be added here. But it depends on which method of authentication you use. You can use simple username/password or JWTs, sessions etc.
    blueprint = Blueprint('api', __name__)

    api = Api(blueprint,
            title='Apis',
            version='1.0',
            description='Task demo',
            doc="/api/info")

    app.register_blueprint(blueprint)

    for ns in all_namespaces:
        api.add_namespace(ns)

    return app


def init_db(app):
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///../tasks_assessment.db"
    db.init_app(app)

app = init_app()


if __name__ == "__main__":
    app.run(debug=True)
