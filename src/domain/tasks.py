from repository.tasks import TasksRepository


DEFAULT_PER_PAGE = 20
DEFAULT_SORT_BY = 'id'


class TasksService:
    tasks_repository = TasksRepository()

    def get_task(self, id):
        task = self.tasks_repository.get(id)
        return task
    
    def get_all_tasks(self):
        tasks = self.tasks_repository.get_all()
        return tasks
    
    def get_filtered_tasks(self, page, per_page, filter, sort_by):
        if page and per_page is None:
            per_page = DEFAULT_PER_PAGE
        
        if sort_by is None:
            sort_by = DEFAULT_SORT_BY

        page = self.tasks_repository.get_by_filter(page, per_page, filter, sort_by)
        return page

    def create_task(self, task):
        created_task = self.tasks_repository.create(task)
        return created_task

    def delete_task(self, id):
        self.tasks_repository.delete(id)

    def update_task(self, id, task):
        # You could add some logic here that limits changes of status for example.
        updated_task = self.tasks_repository.update(id, task)
        return updated_task
