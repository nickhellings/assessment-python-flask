from domain.models.task_status import TaskStatuses


class Task:
    id = None
    name = None
    status = TaskStatuses.NEW
    description = None

    def __init__(self, id=None, name=None, status=None, description=None):
        self.id = id
        self.name = name 
        self.status = status or TaskStatuses.NEW
        self.description = description

    @classmethod
    def from_json(cls, task_json):
        task = cls(id=task_json.get("id"),
            name=task_json.get("name"),
            status=task_json.get("status"),
            description=task_json.get("description"))
        
        return task
