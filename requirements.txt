Flask==3.0.0
flask-restx==1.3.0
Flask-SQLAlchemy==3.1.1
Flask-Migrate==4.0.5
SQLAlchemy==2.0.25
requests==2.31.0