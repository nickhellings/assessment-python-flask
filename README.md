## Name

Assessment Python Flask

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Run this as a flask app. Go to http://localhost:5000/api/info and use the swagger documentation to perform calls.

## Further improvements

Better separation between domain layer and repository layer.
Add authentication and authorization
