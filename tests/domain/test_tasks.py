import pytest
from unittest.mock import patch
from domain.tasks import TasksService


# These tests were generated using chatgpt. Since there is no logic in the domain layer, tests don't really make sense.


@pytest.fixture
def tasks_service():
    return TasksService()

def test_get_task(tasks_service):
    with patch.object(tasks_service.tasks_repository, 'get') as mock_get:
        mock_get.return_value = {'id': 1, 'title': 'Example Task'}
        
        result = tasks_service.get_task(1)

        mock_get.assert_called_once_with(1)
        assert result == {'id': 1, 'title': 'Example Task'}

def test_get_all_tasks(tasks_service):
    with patch.object(tasks_service.tasks_repository, 'get_all') as mock_get_all:
        mock_get_all.return_value = [{'id': 1, 'title': 'Task 1'}, {'id': 2, 'title': 'Task 2'}]
        
        result = tasks_service.get_all_tasks()

        mock_get_all.assert_called_once()
        assert result == [{'id': 1, 'title': 'Task 1'}, {'id': 2, 'title': 'Task 2'}]

def test_get_filtered_tasks(tasks_service):
    with patch.object(tasks_service.tasks_repository, 'get_by_filter') as mock_get_by_filter:
        mock_get_by_filter.return_value = [{'id': 1, 'title': 'Task 1'}, {'id': 2, 'title': 'Task 2'}]

        result = tasks_service.get_filtered_tasks(1, 10, 'example_filter', 'title')

        mock_get_by_filter.assert_called_once_with(1, 10, 'example_filter', 'title')
        assert result == [{'id': 1, 'title': 'Task 1'}, {'id': 2, 'title': 'Task 2'}]

def test_create_task(tasks_service):
    with patch.object(tasks_service.tasks_repository, 'create') as mock_create:
        mock_create.return_value = {'id': 1, 'title': 'New Task'}

        result = tasks_service.create_task({'title': 'New Task'})

        mock_create.assert_called_once_with({'title': 'New Task'})
        assert result == {'id': 1, 'title': 'New Task'}

def test_delete_task(tasks_service):
    with patch.object(tasks_service.tasks_repository, 'delete') as mock_delete:
        tasks_service.delete_task(1)

        mock_delete.assert_called_once_with(1)

def test_update_task(tasks_service):
    with patch.object(tasks_service.tasks_repository, 'update') as mock_update:
        mock_update.return_value = {'id': 1, 'title': 'Updated Task'}

        result = tasks_service.update_task(1, {'title': 'Updated Task'})

        mock_update.assert_called_once_with(1, {'title': 'Updated Task'})
        assert result == {'id': 1, 'title': 'Updated Task'}